using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButtonController : MonoBehaviour
{
    public GameObject pausePanel;

    public void OnPauseButtonClicked()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }
}
