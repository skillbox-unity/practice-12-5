using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Inputs;

[RequireComponent(typeof(BallController))]

public class PlayerInput : MonoBehaviour
{
    private Vector3 movement;
    private BallController ballController;

    // Start is called before the first frame update
    void Start()
    {
        ballController = GetComponent<BallController>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis(GlobalVars.HORIZONTAL_AXIS);
        float vertical = Input.GetAxis(GlobalVars.VERTICAL_AXIS);
        movement = new Vector3(-horizontal, 0, -vertical).normalized;
    }

    private void FixedUpdate()
    {
        ballController.moveBall(movement);
    }
}
