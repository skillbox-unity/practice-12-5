using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddObjectTrigger : MonoBehaviour
{
    [SerializeField] private GameObject actionObject;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            actionObject.SetActive(true);
        }
    }
}
