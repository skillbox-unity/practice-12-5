using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeFloorController : MonoBehaviour
{
    [SerializeField] private int power;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Vector3 direction = collision.transform.position - transform.position;
            collision.rigidbody.AddForce(direction.normalized * power, ForceMode.Impulse);
        }
    }
}
