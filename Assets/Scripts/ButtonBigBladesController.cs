using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBigBladesController : MonoBehaviour
{
    [SerializeField] private NearbyButtonTextController nearbyButtonTextController;

    void Update()
    {
        OnButtonPressed();
    }

    private void OnButtonPressed()
    {
        if (Input.GetKeyDown(KeyCode.E) && nearbyButtonTextController.isNearby)
        {
            GameObject[] allBigBlades = GameObject.FindGameObjectsWithTag("BigBlades");
            foreach (GameObject gameObj in allBigBlades)
            {
                if(gameObj.TryGetComponent<Animator>(out Animator bigBladesAnimator))
                {
                    if (bigBladesAnimator.speed == 1)
                        bigBladesAnimator.speed = 0;
                    else
                        bigBladesAnimator.speed = 1;
                }
            }
        }
    }
}
